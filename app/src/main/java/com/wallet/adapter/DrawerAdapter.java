package com.wallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wallet.models.DrawModel;
import com.wallet.titanium.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DrawerAdapter extends ArrayAdapter<DrawModel> {

    ArrayList<DrawModel> m_listarray;
    Context mContext;

    public DrawerAdapter(Context mcontext, ArrayList<DrawModel> listarray)
    {
        super(mcontext, R.layout.list_view_item_row,listarray);
        this.m_listarray = listarray;
        this.mContext = mcontext;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            convertView = inflater.inflate(R.layout.list_view_item_row, null);

            viewHolder = new ViewHolder();
            viewHolder.imageViewIcon = (ImageView) convertView.findViewById(R.id.imageViewIcon);
            viewHolder.textViewName = (TextView)convertView.findViewById(R.id.textViewName);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        DrawModel drawModel = this.m_listarray.get(position);

        viewHolder.imageViewIcon.setBackgroundResource(R.drawable.icon_email);
        viewHolder.textViewName.setText(drawModel.getName());

        return convertView;
    }

    public class ViewHolder
    {
        ImageView imageViewIcon;
        TextView textViewName;
    }
}
