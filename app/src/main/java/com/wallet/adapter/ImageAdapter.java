package com.wallet.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.wallet.Webservice.UrlService;
import com.wallet.titanium.R;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private static  final String TAG = "Recyleviewadapter";
    public ArrayList<String> imageArray = new ArrayList<>();
    private Context m_context;

    DeleteRequestInterface deleteRequestInterface;

    public ImageAdapter(ArrayList<String> images,Context context)
    {
        imageArray = images;
        m_context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_imagerecyleview, viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Log.e("this is url", UrlService.URL_SERVER + imageArray.get(i));
        Picasso.get()
                .load(UrlService.URL_SERVER + imageArray.get(i))
                .resize(150, 150)
                .centerCrop()
                .into(viewHolder.m_imageview);
        viewHolder.m_deletebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRequestInterface.onDeleteImageRequest(i);
                Log.e("onclick delete button","ok");
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArray.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView m_imageview;
        Button m_deletebutton;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            m_imageview = itemView.findViewById(R.id.item_imageview);
            m_deletebutton = itemView.findViewById(R.id.btn_itemdelete);
        }
    }

    public interface DeleteRequestInterface
    {
        public void onDeleteImageRequest(int position);
    }
}
