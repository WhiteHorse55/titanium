package com.wallet.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wallet.Webservice.UrlService;
import com.wallet.models.SearchItemModel;
import com.wallet.titanium.R;

import java.util.ArrayList;

import tcking.github.com.giraffeplayer2.VideoView;

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnBottomReachedListener onBottomReachedListener;

    ArrayList<SearchItemModel> item_array;
    Context m_context;

    public SearchAdapter(ArrayList<SearchItemModel> images, Context context)
    {
        item_array = images;
        m_context = context;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view =null;
        RecyclerView.ViewHolder viewHolder = null;

        if(i == 0)
        {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_video, viewGroup,false);
            viewHolder = new ViewHolder_Video(view);
        }else{
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_image, viewGroup,false);
            viewHolder = new ViewHolder_Image(view);
        }

        return viewHolder;

    }

    @Override
    public int getItemViewType(int position) {
        SearchItemModel searchItemModel = item_array.get(position);
        String type = searchItemModel.getType();
        if(type.equals("0"))
        {
            return 0;
        }else{
            return 1;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
        SearchItemModel searchItemModel = item_array.get(i);

        switch (viewHolder.getItemViewType()) {
            case 0:
                final ViewHolder_Video viewHolder_video = (ViewHolder_Video) viewHolder;
                viewHolder_video.txt_video_title.setText(searchItemModel.getTitle());
                viewHolder_video.txt_video_company.setText(searchItemModel.getCompany());
                viewHolder_video.txt_count.setText(searchItemModel.getCount_num());
//                viewHolder_video.videoView.setVideoURI(Uri.parse(UrlService.URL_SERVER + searchItemModel.getVideo_url()));
//                viewHolder_video.videoView.setVideoURI(Uri.parse(searchItemModel.getVideo_url()));
                viewHolder_video.videoView.setVideoPath(UrlService.URL_SERVER + searchItemModel.getVideo_url()).setFingerprint(i);
                viewHolder_video.videoView.getPlayer().seekTo(1000);
                viewHolder_video.videoView.getPlayer().pause();
//                viewHolder_video.videoView.getPlayer().setDisplayModel(GiraffePlayer.DISPLAY_FLOAT);
                break;

            case 1:
                ViewHolder_Image viewHolder_image= (ViewHolder_Image) viewHolder;
                viewHolder_image.txt_image_title.setText(searchItemModel.getTitle());
                viewHolder_image.txt_image_company.setText(searchItemModel.getCompany());

//                Picasso.get()
//                        .load(UrlService.URL_SERVER + searchItemModel.getImage_first())
//                        .resize(150, 150)
//                        .centerCrop()
//                        .into(viewHolder_image.image_first);

                Picasso.get()
                        .load(UrlService.URL_SERVER + searchItemModel.getImage_first())
                        .resize(150, 150)
                        .centerCrop()
                        .into(viewHolder_image.image_first);
                Picasso.get()
                        .load(UrlService.URL_SERVER  + searchItemModel.getImage_second())
                        .resize(150, 150)
                        .centerCrop()
                        .into(viewHolder_image.image_second);

                Picasso.get()
                        .load(UrlService.URL_SERVER  + searchItemModel.getImage_third())
                        .resize(150, 150)
                        .centerCrop()
                        .into(viewHolder_image.image_third);
                break;
        }

        if (i == item_array.size() - 1)
        {
            onBottomReachedListener.onBottomReached(i);
        }
    }


    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener)
    {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    @Override
    public int getItemCount() {
        return item_array.size();
    }

    public class ViewHolder_Video extends RecyclerView.ViewHolder {
        TextView txt_video_title;
        VideoView videoView;
        TextView txt_video_company;
        TextView txt_count;
        Button btn_dislike;


        public ViewHolder_Video(@NonNull View itemView) {
            super(itemView);
            txt_video_title = itemView.findViewById(R.id.item_video_title);
            txt_video_company = itemView.findViewById(R.id.item_video_company);
            videoView = itemView.findViewById(R.id.item_video_video);
            txt_count = itemView.findViewById(R.id.item_count);

            btn_dislike = itemView.findViewById(R.id.item_dislike);
        }
    }

    public class ViewHolder_Image extends RecyclerView.ViewHolder {
        TextView txt_image_title;
        ImageView image_first;
        ImageView image_second;
        ImageView image_third;
        TextView txt_image_company;

        public ViewHolder_Image(@NonNull View itemView) {
            super(itemView);
            txt_image_title = itemView.findViewById(R.id.item_image_title);
            txt_image_company = itemView.findViewById(R.id.item_image_company);
            image_first = itemView.findViewById(R.id.item_image_first);
            image_second = itemView.findViewById(R.id.item_image_second);
            image_third = itemView.findViewById(R.id.item_image_third);
        }
    }

    public interface OnBottomReachedListener
    {
        void onBottomReached(int position);
    }

}
