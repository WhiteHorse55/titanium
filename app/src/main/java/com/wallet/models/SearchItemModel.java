package com.wallet.models;

public class SearchItemModel {
    private String id;
    private String title;
    private String company;
    private String created;
    private String count_num;
    private String video_url;
    private String image_first;
    private String image_second;
    private String image_third;
    private String type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCount_num() {
        return count_num;
    }

    public void setCount_num(String count_num) {
        this.count_num = count_num;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getImage_first() {
        return image_first;
    }

    public void setImage_first(String image_first) {
        this.image_first = image_first;
    }

    public String getImage_second() {
        return image_second;
    }

    public void setImage_second(String image_second) {
        this.image_second = image_second;
    }

    public String getImage_third() {
        return image_third;
    }

    public void setImage_third(String image_third) {
        this.image_third = image_third;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
