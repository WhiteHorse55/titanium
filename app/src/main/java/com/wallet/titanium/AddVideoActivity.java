package com.wallet.titanium;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wallet.Utils.MyUtils;
import com.wallet.Webservice.UrlService;

import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import tcking.github.com.giraffeplayer2.VideoView;

public class AddVideoActivity extends App implements View.OnClickListener {

    VideoView videoView;

    Boolean isPlay;

    EditText edit_title;
    EditText edit_company;

    File _videofile;

    String video_url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);

        avLoadingIndicatorView = findViewById(R.id.loadingview);

        Button btn_addvideo = findViewById(R.id.btn_toolbar_add);
        Button btn_back = findViewById(R.id.btn_toolbar_back);
        Button btn_upload = findViewById(R.id.btn_uploadvideo);
        Button btn_galleryvideo = findViewById(R.id.btn_galleryvideo);
        Button btn_galleryvideoremove = findViewById(R.id.btn_galleryvideoremove);


        edit_title = findViewById(R.id.edit_add_title);
        edit_company = findViewById(R.id.edit_add_companyname);

        TextView title_toolbar = findViewById(R.id.toolbar_title);
        title_toolbar.setText("Create video Adv");

        btn_addvideo.setVisibility(View.GONE);
        btn_addvideo.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_upload.setOnClickListener(this);
        btn_galleryvideo.setOnClickListener(this);
        btn_galleryvideoremove.setOnClickListener(this);

        videoView = findViewById(R.id.videoview);

        isPlay = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_galleryvideo:
                    getVideoFromGallery();
                break;

            case R.id.btn_toolbar_back:
                    finish();
                break;


            case R.id.btn_uploadvideo:
                    uploadVideo();
                break;

            case R.id.btn_galleryvideoremove:
                break;

            default:
               break;
        }
    }

    private void uploadVideo()
    {
        if(_videofile == null)
        {
            MyUtils.showToast(getBaseContext(), "Please choose video file first");
        }else{
            if(!validation())
            {
                return;
            }
            uploadVideoPostData();
        }
    }

    private void uploadvideotoserver()
    {
        startAnim();
        String url = UrlService.URL_FILEUPLOAD;

        AndroidNetworking.upload(url)
                         .addMultipartFile("file",_videofile)
                         .setTag("fileupload")
                         .setPriority(Priority.HIGH)
                         .build()
                         .getAsJSONObject(new JSONObjectRequestListener() {
                             @Override
                             public void onResponse(JSONObject response) {
                                 try {
                                     Log.e("response", response.toString());
                                     stopAnim();
                                     if(response.getString("result").equals("0"))
                                     {
                                        MyUtils.showToast(getBaseContext(), response.getString("message"));
                                     }else
                                     {
                                         video_url = response.getString("message");
                                         playonlinevideo();
                                     }


                                 } catch (JSONException e) {
                                     stopAnim();
                                     e.printStackTrace();
                                 }
                             }

                             @Override
                             public void onError(ANError anError) {
                                stopAnim();
                                MyUtils.showToast(getBaseContext(), anError.getErrorDetail().toString());
                             }
                         });
    }

    private void uploadVideoPostData()
    {
        startAnim();
        String url = UrlService.URL_CREATEADEVERT;
        AndroidNetworking.post(url)
                .addBodyParameter("title",edit_title.getText().toString())
                .addBodyParameter("company",edit_company.getText().toString())
                .addBodyParameter("type","0")
                .addBodyParameter("image1","")
                .addBodyParameter("image2","")
                .addBodyParameter("image3","")
                .addBodyParameter("video",video_url)
                .setTag("fileupload")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            stopAnim();
                            if(response.getString("result").equals("1"))
                            {
                                finish();
                            }else{
                                MyUtils.showToast(getBaseContext(), response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            stopAnim();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        stopAnim();
                        MyUtils.showToast(getBaseContext(), anError.toString());
                    }
                });
    }

    private Boolean validation()
    {
        Boolean valid = true;
        String title = edit_title.getText().toString();
        String companyname = edit_company.getText().toString();

        if(title.isEmpty())
        {
            edit_title.setError("Enter your last name");
            valid = false;
        }else{
            edit_title.setError(null);
        }


        if(companyname.isEmpty())
        {
            edit_company.setError("Enter your last name");
            valid = false;
        }else{
            edit_company.setError(null);
        }

        return valid;
    }

    private void getVideoFromGallery()
    {
        new VideoPicker.Builder(AddVideoActivity.this)
                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                .directory(VideoPicker.Directory.DEFAULT)
                .extension(VideoPicker.Extension.MP4)
                .enableDebuggingMode(true)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {

            List<String> mPaths =  data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);

            _videofile = new File(mPaths.get(0));
            uploadvideotoserver();
            //Your Code
        }
    }



    private void playonlinevideo()
    {
        String videoserverurl = UrlService.URL_SERVER + video_url;
        videoView.setVideoPath(videoserverurl);
        playVideo();
    }


    private void pauseVideo()
    {
        videoView.getPlayer().pause();
    }

    private void playVideo()
    {
        videoView.getPlayer().start();
    }

    private void resumeVideo()
    {
        videoView.getPlayer().start();
    }
}
