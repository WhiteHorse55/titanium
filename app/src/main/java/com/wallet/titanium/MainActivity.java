package com.wallet.titanium;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.wallet.Fragment.BusinessFragment;
import com.wallet.Fragment.ChatFragment;
import com.wallet.Fragment.HomeFragment;
import com.wallet.Fragment.ProfileFragment;
import com.wallet.Fragment.ScanFragment;
import com.wallet.Fragment.SearchFragment;
import com.wallet.adapter.DrawerAdapter;
import com.wallet.models.DrawModel;

import java.util.ArrayList;

public class MainActivity extends App  implements View.OnClickListener,
        BottomNavigationView.OnNavigationItemSelectedListener,
        SearchFragment.OnFragmentInteractionListener,
        ScanFragment.OnFragmentInteractionListener,
        ChatFragment.OnFragmentInteractionListener,
        BusinessFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener {


    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout drawerLayout;
    private ListView mDrawerList;

    ActionBarDrawerToggle   mDrawerToggle;

    ArrayList<DrawModel> list_drawer;
    Toolbar toolbar;


    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(SearchFragment.newInstance("test","test"));
        bottomNavigationView = findViewById(R.id.bottomnavigationview);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    private boolean loadFragment(Fragment fragment)
    {
        //switching fragment
        if (fragment != null)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
            return  true;
        }

        return  false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId())
        {
            case R.id.navigation_search:
                    fragment = SearchFragment.newInstance("","");
                break;

            case R.id.navigation_scan:
                    fragment = ScanFragment.newInstance("","");
                break;

            case R.id.navigation_chat:
                    fragment = ChatFragment.newInstance("","");
                break;

            case R.id.navigation_business:
                    fragment = BusinessFragment.newInstance("","");
                break;

            case R.id.navigation_profile:
                    fragment = ProfileFragment.newInstance("","");
                break;
        }
        return loadFragment(fragment);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
