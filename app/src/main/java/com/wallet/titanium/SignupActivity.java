package com.wallet.titanium;

import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.hbb20.CountryCodePicker;
import com.juanpabloprado.countrypicker.CountryPicker;
import com.juanpabloprado.countrypicker.CountryPickerListener;
import com.libizo.CustomEditText;
import com.wallet.Utils.Constant;
import com.wallet.Utils.MyUtils;
import com.wallet.Webservice.UrlService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class SignupActivity extends App implements View.OnClickListener, TimePicker.OnTimeChangedListener, DatePickerDialog.OnDateSetListener {



    CountryCodePicker countryCodePicker;

    Button btn_verify;

    Boolean isVerified;

    CustomEditText _edit_email;
    CustomEditText _edit_password;
    CustomEditText _edit_firstname;
    CustomEditText _edit_lastname;
    TextView _edit_dob;
    CustomEditText _edit_city;
    CustomEditText _edit_stat;
    CustomEditText _edit_phone;

    TextView txt_birth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        isVerified = false;
        avLoadingIndicatorView = findViewById(R.id.loadingview);

        countryCodePicker = findViewById(R.id.signup_countrypicker);

        Button btn_done = findViewById(R.id.signup_done);
        btn_verify = findViewById(R.id.signup_verify_button);

        btn_done.setOnClickListener(this);
        btn_verify.setOnClickListener(this);

        //edittext setting
        _edit_email = findViewById(R.id.edit_signup_email);
        _edit_password = findViewById(R.id.edit_signup_pass);
        _edit_firstname = findViewById(R.id.edit_signup_firstname);
        _edit_lastname = findViewById(R.id.edit_signup_lastname);
        _edit_city = findViewById(R.id.edit_signup_city);
        _edit_dob = findViewById(R.id.edit_signup_dob);
        _edit_stat = findViewById(R.id.edit_signup_state);
        _edit_phone = findViewById(R.id.edit_signup_phone);

        txt_birth = findViewById(R.id.txt_signup_birth);
        txt_birth.setOnClickListener(this);
        _edit_dob.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        disableStatusBar();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signup_done:
                signup();
                break;

            case R.id.txt_signup_birth:
                    showDatePicker();
                break;

            case R.id.edit_signup_dob:
                    showCountryPicker();
                break;

            case R.id.signup_verify_button:
                    onclickverification();
                break;
        }
    }

    private void showDatePicker()
    {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                SignupActivity.this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.setAccentColor("#504bbe");
        dpd.setCancelColor("#504bbe");
        dpd.show(getSupportFragmentManager(),"DatePickerDialog");
    }

    private void showCountryPicker()
    {
        CountryPicker picker = CountryPicker.getInstance("Select Country", new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code) {
//                Toast.makeText(getBaseContext(), "Name: " + name, Toast.LENGTH_SHORT).show();
                _edit_dob.setText(name);
                DialogFragment dialogFragment =
                        (DialogFragment) getSupportFragmentManager().findFragmentByTag("CountryPicker");
                dialogFragment.dismiss();
            }
        });

        picker.show(getSupportFragmentManager(), "CountryPicker");
    }

//    public void getcountrycode()
//    {
//        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
//        String countryCodeValue = tm.getNetworkCountryIso();
//        Log.e("this is country code", countryCodeValue);
//        countryCodePicker.setCountryForNameCode(countryCodeValue);
//    }

    public void onclickverification()
    {
        if(_edit_phone.getText().toString().equals(""))
        {
            MyUtils.showToast(getBaseContext(), Constant.TXT_EMPTY_PHONENUMBER);
        }else{
            String sendnumber = getPhonenumber();
            Log.e("sendnumber: ",sendnumber );
            sendPhoneVerificationRequest(sendnumber);
        }

    }

    //get phonenumber from country picker and edit text;
    private String getPhonenumber()
    {
        String countrycode = this.countryCodePicker.getSelectedCountryCode();
        String number = _edit_phone.getText().toString();
        String sendnumber = countrycode + number;
        return sendnumber;
    }



    private void sendPhoneVerificationRequest(String sendnumber)
    {
        startAnim();
        String url = UrlService.URL_PHONEVERIFICATION;

        AndroidNetworking.post(url)
                .addBodyParameter("phonenumber", sendnumber)
                .setTag("phonenumberverify")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            stopAnim();
                            String str_result = response.getString("result");
                            if (str_result.equals("1"))
                            {
                                String code = response.getString("code");
                                Log.e("this is code", code);
                                gotoPinActivity(code);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        stopAnim();
                        MyUtils.showToast(getBaseContext(), anError.toString());
                    }
                });
    }

    private void gotoPinActivity(String code)
    {
        Intent intent = new Intent(SignupActivity.this, PinActivity.class);
        intent.putExtra("code", code);
        startActivityForResult(intent,Constant.REQUEST_CODE);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);

        if(requestCode == Constant.REQUEST_CODE)
        {
            Boolean status = intent.getBooleanExtra("status", true);
            if(status)
            {
                _edit_phone.setEnabled(false);
                isVerified = true;
                btn_verify.setBackgroundResource(R.drawable.verify);
            }else{
                _edit_phone.setEnabled(true);
                isVerified = false;
                btn_verify.setBackgroundResource(R.drawable.unverify);
            }
        }
    }

    private void signup()
    {
        if(!validate())
        {
            return;
        }
        communicatewithserver();
    }

    private void communicatewithserver()
    {
        startAnim();
        String email = _edit_email.getText().toString();
        String password = _edit_password.getText().toString();
        String firstname = _edit_firstname.getText().toString();
        String lastname = _edit_lastname.getText().toString();
        String dob = _edit_dob.getText().toString();
        String city = _edit_city.getText().toString();
        String state = _edit_stat.getText().toString();
        String phone = getPhonenumber();

        String url = UrlService.URL_SIGNUP;

        AndroidNetworking.post(url)
                .addBodyParameter("email", email)
                .addBodyParameter("password",password)
                .addBodyParameter("firstname",firstname)
                .addBodyParameter("lastname",lastname)
                .addBodyParameter("phonenumber",phone)
                .addBodyParameter("country",dob)
                .addBodyParameter("city",city)
                .addBodyParameter("state",state)
                .setTag("Signup")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopAnim();
                        try {
                            if (response.getString("result").equals("1"))
                            {
                                MyUtils.showToast(getBaseContext(),response.getString("message"));
                                finish();
                            }else{
                                MyUtils.showToast(getBaseContext(),response.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            stopAnim();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        stopAnim();
                        MyUtils.showToast(getBaseContext(), anError.toString());
                    }
                });
    }

    //validation signup form
    public boolean validate() {
        boolean valid = true;

        String email = _edit_email.getText().toString();
        String password = _edit_password.getText().toString();
        String firstname = _edit_firstname.getText().toString();
        String lastname = _edit_lastname.getText().toString();
        String dob = _edit_dob.getText().toString();
        String city = _edit_city.getText().toString();
        String state = _edit_stat.getText().toString();
        String phone = _edit_phone.getText().toString();


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _edit_email.setError("enter a valid email address");
            valid = false;
        } else {
            _edit_email.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _edit_password.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _edit_password.setError(null);
        }

        if(firstname.isEmpty())
        {
            _edit_firstname.setError("Enter your first name");
            valid = false;
        }else{
            _edit_firstname.setError(null);
        }

        if(lastname.isEmpty())
        {
            _edit_lastname.setError("Enter your last name");
            valid = false;
        }else{
            _edit_lastname.setError(null);
        }

        if(dob.isEmpty())
        {
            _edit_dob.setError("Enter your dob");
            valid = false;
        }else{
            _edit_dob.setError(null);
        }


        if(city.isEmpty())
        {
            _edit_city.setError("Enter your city");
            valid = false;
        }else{
            _edit_city.setError(null);
        }


        if(state.isEmpty())
        {
            _edit_stat.setError("Enter your state");
            valid = false;
        }else{
            _edit_stat.setError(null);
        }

        if(phone.isEmpty())
        {
            _edit_phone.setError("Enter a Phone number");
            valid = false;
        }else{
            _edit_phone.setError(null);
        }


        if(!isVerified)
        {
            MyUtils.showToast(getBaseContext(), Constant.ERROR_VERIFYPHONE);
            valid = false;
        }

        return valid;
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        txt_birth.setText(String.valueOf(year)+ "/"+ String.valueOf(monthOfYear + 1)+"/"+ String.valueOf(dayOfMonth));
    }
//
//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        txt_birth.setText(String.valueOf(year) + String.valueOf(monthOfYear) + String.valueOf(dayOfMonth));
//    }
}
