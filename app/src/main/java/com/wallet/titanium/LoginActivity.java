package com.wallet.titanium;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.wallet.Utils.MyUtils;
import com.wallet.Webservice.UrlService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class LoginActivity extends App  implements View.OnClickListener{

    private CallbackManager mCallbackManager;


    private static final String TAG = "SigninActivity";
    EditText _emailText;
    EditText _passwordText;
    Button btn_login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        //facebook callbackmanger
        mCallbackManager = CallbackManager.Factory.create();

        avLoadingIndicatorView = findViewById(R.id.loadingview);

        btn_login = findViewById(R.id.btn_signin);
        Button btn_signup = findViewById(R.id.btn_signup);


        Button btn_fakebutton = findViewById(R.id.fb_fake_button);
        btn_fakebutton.setOnClickListener(this);

        btn_login.setOnClickListener(this);
        btn_signup.setOnClickListener(this);

        _emailText = findViewById(R.id.input_email);
        _passwordText = findViewById(R.id.input_password);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case R.id.btn_signin:
                login();
                break;

            case R.id.btn_signup:
                Intent intent = new Intent(this, ScanActivity.class);
                startActivity(intent);
                break;
            case R.id.fb_fake_button:
                facebookLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void facebookLogin() {
        LoginButton btn_facebook = findViewById(R.id.fb_login_button);
        btn_facebook.performClick();

        List< String > permissionNeeds = Arrays.asList("user_photos", "email",
                "user_birthday", "public_profile");
        btn_facebook.setReadPermissions(permissionNeeds);

        btn_facebook.registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        String accessToken = loginResult.getAccessToken()
                                .getToken();
                        Log.i("accessToken", accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {@Override
                                public void onCompleted(JSONObject object,
                                                        GraphResponse response) {
                                    try {
                                        String fb_id = object.getString("id");
                                        Log.e("Facebookresponse",response.toString());
                                        String profile_pic = "http://graph.facebook.com/" + fb_id + "/picture";

                                        String fb_firstname = object.getString("first_name");
                                        String fb_lastname = object.getString("last_name");
                                        String fb_email = object.getString("email");

                                        signupwithFB(fb_email, fb_firstname, fb_lastname);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields",
                                "id,name,email,gender, birthday,first_name, last_name");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
//                        if (AccessToken.getCurrentAccessToken() == null) {
//                            return; // already logged out
//                        }
//                        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
//                                .Callback() {
//                            @Override
//                            public void onCompleted(GraphResponse graphResponse) {
//                                LoginManager.getInstance().logOut();
//                                List < String > permissionNeeds = Arrays.asList("user_photos", "email",
//                                        "user_birthday", "public_profile");
//                                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, permissionNeeds);
//                                facebookLogin();
//                            }
//                        }).executeAsync();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        AccessToken.setCurrentAccessToken(null);
                        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("user_photos", "email",
                                "user_birthday", "public_profile"));
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void gotoMainPage()
    {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }


    private void signupwithFB(String email, String fname, String lname)
    {
        startAnim();

        String url = UrlService.URL_FACEBOOKSIGNUP;

        AndroidNetworking.post(url)
                .addBodyParameter("email", email)
                .addBodyParameter("firstname",fname)
                .addBodyParameter("lastname",lname)
                .setTag("FBLogin")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopAnim();
                        try {
                            MyUtils.showToast(getBaseContext(),response.getString("message"));
                            gotoMainPage();

                        } catch (JSONException e) {
                            stopAnim();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        stopAnim();
                        MyUtils.showToast(getBaseContext(), anError.toString());
                    }
                });
    }


    public void login()
    {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        btn_login.setEnabled(false);
        communicatewithserver();
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    public void onLoginSuccess() {
        btn_login.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        MyUtils.showToast(getBaseContext(), "Login failed");
        btn_login.setEnabled(true);
    }

    //communicate server
    public void communicatewithserver()
    {
        startAnim();
        String str_email = _emailText.getText().toString();
        String str_password = _passwordText.getText().toString();

        String url = UrlService.URL_LOGIN;

        AndroidNetworking.post(url)
                .addBodyParameter("email", str_email)
                .addBodyParameter("password",str_password)
                .setTag("Login")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopAnim();
                        try {
                            if (response.getString("result").equals("1"))
                            {
                                gotoMainPage();
                            }else{
                                MyUtils.showToast(getBaseContext(),response.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        stopAnim();
                        MyUtils.showToast(getBaseContext(), anError.toString());
                    }
                });
    }
}
