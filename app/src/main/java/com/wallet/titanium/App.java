package com.wallet.titanium;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Window;
import android.view.WindowManager;

import com.wallet.permission.MarshMallowPermission;
import com.wang.avi.AVLoadingIndicatorView;

public class App extends AppCompatActivity {

    AVLoadingIndicatorView avLoadingIndicatorView;
    private MarshMallowPermission marshMallowPermission;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        disableStatusBar();
    }

    public void disableStatusBar()
    {
//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    public void removeActionbarTitle()
    {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
    }


    void startAnim(){
        avLoadingIndicatorView.show();
        // or avi.smoothToShow();
    }

    void stopAnim(){
        avLoadingIndicatorView.hide();
        // or avi.smoothToHide();
    }


    private void checkRuntimePermission()
    {
        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage(MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE);
        }

        if(!marshMallowPermission.checkPermissionForCamera())
        {
            marshMallowPermission.requestPermissionForCamera();
        }
    }

}
