package com.wallet.titanium;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.libizo.CustomEditText;
import com.wallet.Utils.Constant;
import com.wallet.Utils.MyUtils;

public class PinActivity extends App implements View.OnClickListener {

    String pin_code;
    CustomEditText edit_verifycode;
    Button btn_verifycode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pin_code = getIntent().getStringExtra("code");
        }

        edit_verifycode = findViewById(R.id.edit_pin_verifycode);

        Button btn_verifycode = findViewById(R.id.btn_pin_verifycode);
        Button btn_cancel = findViewById(R.id.btn_pin_cancel);

        btn_verifycode.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_pin_verifycode:
                    if(edit_verifycode.getText().toString().equals(pin_code))
                    {
                        sendsuccessrequest(true);
                    }else{
                        MyUtils.showToast(getBaseContext(), Constant.ERROR_PINCODE);
                    }

                break;

            case R.id.btn_pin_cancel:
                    sendsuccessrequest(false);
                break;

            default:
                break;
        }
    }

    private void sendsuccessrequest(Boolean status)
    {
        Intent intent = new Intent();
        intent.putExtra("status", status);
        if(status)
        {
            //if success
            setResult(Constant.STATUS_SUCESS, intent);
        }else{
            //if fail
            setResult(Constant.STATUS_FAIL,intent);
        }
        finish();
    }
}
