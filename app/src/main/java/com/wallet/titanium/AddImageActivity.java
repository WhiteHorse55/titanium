package com.wallet.titanium;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wallet.Utils.MyUtils;
import com.wallet.Webservice.UrlService;
import com.wallet.adapter.ImageAdapter;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AddImageActivity extends App implements View.OnClickListener,ImageAdapter.DeleteRequestInterface {


    RecyclerView recyclerView;
    ArrayList<String> imageArrayList;
    ImageAdapter imageAdapter;

    EditText edit_title;
    EditText edit_company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image);
        imageArrayList = new ArrayList<>();
        avLoadingIndicatorView = findViewById(R.id.loadingview);

        Button btn_addimage = findViewById(R.id.btn_toolbar_add);
        Button btn_back = findViewById(R.id.btn_toolbar_back);
        Button btn_galleryimage = findViewById(R.id.btn_galleryimage);
        Button btn_upload = findViewById(R.id.add_image_uploadbutton);

        btn_addimage.setOnClickListener(this);
        btn_upload.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_galleryimage.setOnClickListener(this);

        edit_title = findViewById(R.id.add_image_title);
        edit_company = findViewById(R.id.add_image_company);

        TextView title_toolbar = findViewById(R.id.toolbar_title);
        title_toolbar.setText("Create Photo Adv");

        btn_addimage.setVisibility(View.GONE);
//        btn_back.setVisibility(View.GONE);
        initRecyleView();
    }


    private void initRecyleView()
    {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView = findViewById(R.id.image_recyleview);
        recyclerView.setLayoutManager(layoutManager);

        imageAdapter = new ImageAdapter(imageArrayList, getBaseContext());
        recyclerView.setAdapter(imageAdapter);
    }

    private void getImagesFromPicker()
    {
        new ImagePicker.Builder(AddImageActivity.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.JPG)
                .scale(300, 300)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            uploadImageToServer(mPaths.get(0));
        }
    }

    //upload one image to server
    private void uploadImageToServer(final String imageurl)
    {
        startAnim();
        String url = UrlService.URL_FILEUPLOAD;

        Log.e("urlinfo==>", imageurl);
        AndroidNetworking.upload(url)
                .addMultipartFile("file",new File(imageurl))
                .setTag("fileupload")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("response==>",response.toString());
                            stopAnim();
                            imageArrayList.add(response.getString("message"));
                            imageAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            stopAnim();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        stopAnim();
                        MyUtils.showToast(getBaseContext(), anError.toString());
                    }
                });
    }

    private void uploadImagesWithData()
    {

        if(imageArrayList.size() != 3)
        {
            MyUtils.showToast(getBaseContext(), "Please add three images");
        }else{

            if(!validation())
            {
                return;
            }

            startAnim();
            String url = UrlService.URL_CREATEADEVERT;
            AndroidNetworking.post(url)
                    .addBodyParameter("title",edit_title.getText().toString())
                    .addBodyParameter("company",edit_company.getText().toString())
                    .addBodyParameter("type","1")
                    .addBodyParameter("image1",imageArrayList.get(0))
                    .addBodyParameter("image2",imageArrayList.get(1))
                    .addBodyParameter("image3",imageArrayList.get(2))
                    .addBodyParameter("video","")
                    .setTag("imageupload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                stopAnim();
                                if(response.getString("result").equals("1"))
                                {
                                    finish();
                                }else{
                                    MyUtils.showToast(getBaseContext(), response.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                stopAnim();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            stopAnim();
                            MyUtils.showToast(getBaseContext(), anError.toString());
                        }
                    });
        }

    }

    private Boolean validation()
    {
        Boolean valid = true;
        String title = edit_title.getText().toString();
        String companyname = edit_company.getText().toString();

        if(title.isEmpty())
        {
            edit_title.setError("Enter your last name");
            valid = false;
        }else{
            edit_title.setError(null);
        }


        if(companyname.isEmpty())
        {
            edit_company.setError("Enter your last name");
            valid = false;
        }else{
            edit_company.setError(null);
        }

        return valid;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_galleryimage:
                getImagesFromPicker();
                break;

            case R.id.add_image_uploadbutton:
                    uploadImagesWithData();
                break;

            case R.id.btn_toolbar_back:
                    finish();
                break;
        }
    }


    @Override
    public void onDeleteImageRequest(int position) {
        String str = imageArrayList.get(position);
        imageArrayList.remove(str);
        imageAdapter.notifyDataSetChanged();
    }
}
