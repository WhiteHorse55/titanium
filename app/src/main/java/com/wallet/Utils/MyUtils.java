package com.wallet.Utils;
import android.content.Context;
import android.widget.Toast;

public class MyUtils {

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
