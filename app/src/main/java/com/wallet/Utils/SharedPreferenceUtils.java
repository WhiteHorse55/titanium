package com.wallet.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtils {

    private static String tag = SharedPreferenceUtils.class.getSimpleName();
    /**
     * 保存在手机里面的文件名
     */
    private final static String SP_NAME = "share_data";
    private static SharedPreferences sp;


    public interface SharePreKEY {
        String username="username";
        String password="password";
        String userId = "userId";
        String countryId = "countryId";
        String townId = "townId";
        String useremail = "useremail";
        String userToken = "userToken";
        String userNotification = "userNotification";

        String badgetNumber = "badgetNumber";
        String badge = "badge";

    }

    public static void saveBoolean(Context context, String key, boolean value) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        sp.edit().putBoolean(key, value).commit();
    }
    public static void saveString(Context context, String key, String value) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        sp.edit().putString(key, value).commit();
    }
    public static void clear(Context context) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        sp.edit().clear().commit();
    }

//    public static void saveArray(Context context, String key, ArrayList<BadgeModel> bgmodel)
//    {
//        if (sp == null)
//            sp = context.getSharedPreferences(SP_NAME, 0);
//
//    }
//
//    public static void saveBadgeArray(Context context,String key, ArrayList<BadgeModel> array) {
//
//        if (sp == null)
//            sp = context.getSharedPreferences(SP_NAME, 0);
//        Gson gson = new Gson();
//        String json = gson.toJson(array);
//        sp.edit().putString(key, json).commit();
//
//    }

//    public static ArrayList getBadgeArray(Context context, String key, String defultvlue)
//    {
//        if (sp == null)
//            sp = context.getSharedPreferences(SP_NAME, 0);
//        Gson gson = new Gson();
//        String json = sp.getString(key, "");
//        Type type = new TypeToken<ArrayList<BadgeModel>>() {}.getType();
//        ArrayList<BadgeModel> infoList = gson.fromJson(json, type);
//
//        return infoList;
//    }

    public static void saveLong(Context context, String key, long value) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        sp.edit().putLong(key, value).commit();
    }
    public static void saveInt(Context context, String key, int value) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        sp.edit().putInt(key, value).commit();
    }
    public static void saveFloat(Context context, String key, float value) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        sp.edit().putFloat(key, value).commit();
    }
    public static String getString(Context context, String key, String defValue) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        return sp.getString(key, defValue);
    }
    public static int getInt(Context context, String key, int defValue) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        return sp.getInt(key, defValue);
    }
    public static long getLong(Context context, String key, long defValue) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        return sp.getLong(key, defValue);
    }
    public static float getFloat(Context context, String key, float defValue) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        return sp.getFloat(key, defValue);
    }
    public static boolean getBoolean(Context context, String key, boolean defValue) {
        if (sp == null)
            sp = context.getSharedPreferences(SP_NAME, 0);
        return sp.getBoolean(key, defValue);
    }

}
