package com.wallet.Utils;

public class Constant {

    public static String TXT_EMPTY_PHONENUMBER = "Please fill the phone number!";
    public static String ERROR_PINCODE = "Please insert a correct code";
    public static String ERROR_VERIFYPHONE = "Please verify your phonenumber. ";

    public static int STATUS_SUCESS = 200;
    public static int STATUS_FAIL = 201;

    public static int REQUEST_CODE = 100;
}
