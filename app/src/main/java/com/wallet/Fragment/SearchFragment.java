package com.wallet.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.wallet.Utils.MyUtils;
import com.wallet.Webservice.UrlService;
import com.wallet.adapter.SearchAdapter;
import com.wallet.models.SearchItemModel;
import com.wallet.titanium.AddImageActivity;
import com.wallet.titanium.AddVideoActivity;
import com.wallet.titanium.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    FloatingActionMenu floatingActionMenu;

    ArrayList<SearchItemModel> advertArray;
    SearchAdapter searchAdapter;

    RecyclerView searchRecyleView;
    SwipeRefreshLayout swipeRefreshLayout;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view  = inflater.inflate(R.layout.fragment_search, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbarwithsearch);

//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);

        FloatingActionButton fab_video = view.findViewById(R.id.fab_video);
        fab_video.setOnClickListener(this);

        FloatingActionButton fab_image = view.findViewById(R.id.fab_image);
        fab_image.setOnClickListener(this);

        floatingActionMenu = view.findViewById(R.id.menu_fab);
        floatingActionMenu.close(true);

        advertArray = new ArrayList<>();

        searchRecyleView = view.findViewById(R.id.searchrecyleview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_container);
        searchRecyleView = view.findViewById(R.id.searchrecyleview);

        setSwipeRefersh();


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fab_video:
                onclickFabVideoButton();
                break;

            case R.id.fab_image:
                onclickFabImagebutton();
                break;
        }
    }

    private void onclickFabImagebutton()
    {
        Intent intent = new Intent(getActivity(), AddImageActivity.class);
        startActivity(intent);
    }

    private void onclickFabVideoButton()
    {
        Intent intent = new Intent(getActivity(), AddVideoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        advertArray = new ArrayList<>();
        getAdertDataFromServer("0");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void setSwipeRefersh()
    {
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    private void getAdertDataFromServer(String startpoint)
    {
        String url = UrlService.URL_GETADEVERT;

        AndroidNetworking.post(url)
                .addBodyParameter("length", "10")
                .addBodyParameter("start",startpoint)
                .setTag("getadvert")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = (JSONArray) response.get("message");
                            for (int i = 0 ; i < jsonArray.length(); i++)
                            {
                                JSONObject object = (JSONObject) jsonArray.get(i);
                                SearchItemModel searchItemModel = new SearchItemModel();
                                searchItemModel.setTitle(object.getString("title"));
                                searchItemModel.setId(object.getString("id"));
                                searchItemModel.setCompany(object.getString("company"));
                                searchItemModel.setType(object.getString("type"));
                                searchItemModel.setImage_first(object.getString("image1"));
                                searchItemModel.setImage_second(object.getString("image2"));
                                searchItemModel.setImage_third(object.getString("image3"));
                                searchItemModel.setVideo_url(object.getString("video"));
                                searchItemModel.setCount_num(object.getString("view_count"));
                                searchItemModel.setCreated(object.getString("timestamp"));
                                advertArray.add(searchItemModel);
                                if(i == jsonArray.length() - 1)
                                {
                                    swipeRefreshLayout.setRefreshing(false);
                                    searchAdapter.notifyDataSetChanged();
                                }

                            }
                        } catch (JSONException e) {
                            swipeRefreshLayout.setRefreshing(false);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        swipeRefreshLayout.setRefreshing(false);
                        MyUtils.showToast(getActivity(), anError.toString());
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        floatingActionMenu.close(true);
        advertArray = new ArrayList<>();
        initlistview();
        getAdertDataFromServer("0");
    }

    private void initlistview()
    {
        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);

        searchRecyleView.setLayoutManager(layoutManager);
        searchAdapter = new SearchAdapter(advertArray, getContext());
        searchRecyleView.setAdapter(searchAdapter);

        //get new data when scroll last item
        searchAdapter.setOnBottomReachedListener(new SearchAdapter.OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                Log.e("reached lastitem", String.valueOf(position));
            }
        });
    }
}
