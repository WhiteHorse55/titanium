package com.wallet.Webservice;

public class UrlService {
//    public static String URL_SERVER = "http://192.168.1.18:8080/titanium/";
    public static String URL_SERVER = "http://withloveapp.a2hosted.com/titanium/";
    public static String URL_LOGIN = URL_SERVER + "api/login_email";
    public static String URL_SIGNUP = URL_SERVER + "api/signup";
    public static String URL_FACEBOOKSIGNUP = URL_SERVER + "api/login_social";

    public static String URL_FILEUPLOAD= URL_SERVER + "api_advertising/upload_file";
    public static String URL_CREATEADEVERT= URL_SERVER + "api_advertising/create";
    public static String URL_GETADEVERT= URL_SERVER + "api_advertising/get_page";

    public static String URL_PHONEVERIFICATION = URL_SERVER + "verify_phonenumber";
}
